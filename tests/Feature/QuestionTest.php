<?php

namespace Tests\Feature;

use App\Question;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class QuestionTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    private $accessToken;

    protected function setUp(): void
    {
        parent::setUp(); // TODO: Change the autogenerated stub

        \Artisan::call('passport:install');

        $this->user = factory(User::class)->create();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->post('/api/register', [
            'name' => 'name',
            'email' => 'test@test.com',
            'password' => 'password'
        ])->getContent();

        $response = json_decode($response);

        $this->user = (object)[
            'id' => $response->user->id,
            'name' => $response->user->name,
            'email' => $response->user->name,
            'access_token' => $response->access_token,
        ];
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function an_unauthenticated_user_can_not_create_a_question()
    {
        //$this->withoutExceptionHandling();
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->post('/api/questions', $this->data());

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function an_authenticated_user_can_add_a_question()
    {
        $this->withoutExceptionHandling();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'AUTHORIZATION' => 'Bearer ' . $this->user->access_token,
        ])->post('/api/questions', $this->data());

        $question = Question::first();

        $this->assertCount(1, Question::all());
        $this->assertEquals('Question?', $question->question);
        $this->assertEquals('Answer', $question->answer);
        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJson([
            'data' => [
                'question_id' => $question->id,
            ],
            'links' => [
                'self' => $question->path(),
            ]
        ]);
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function a_question_can_be_retrieved()
    {
        $question = factory(Question::class)->create(['user_id' => $this->user->id]);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'AUTHORIZATION' => 'Bearer ' . $this->user->access_token,
        ])->get('/api/questions/' . $question->id);

        $response->assertJson([
            'data' => [
                'question_id' => $question->id,
                'question' => $question->question,
                'answer' => $question->answer,
                'last_updated' => $question->updated_at->diffForHumans(),
            ]
        ]);
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function a_question_can_be_updated()
    {
        $question = factory(Question::class)->create(['user_id' => $this->user->id]);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'AUTHORIZATION' => 'Bearer ' . $this->user->access_token,
        ])->patch('/api/questions/' . $question->id, $this->data());

        $question = $question->fresh();

        $this->assertCount(1, Question::all());
        $this->assertEquals('Question?', $question->question);
        $this->assertEquals('Answer', $question->answer);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson([
            'data' => [
                'question_id' => $question->id,
            ],
            'links' => [
                'self' => $question->path(),
            ]
        ]);
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function only_the_owner_of_the_question_can_update_the_question()
    {
        $question = factory(Question::class)->create();

        $anotherUser = factory(User::class)->create();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'AUTHORIZATION' => 'Bearer ' . $this->user->access_token,
        ])->patch('/api/questions/' . $question->id, array_merge($this->data()));

        $response->assertStatus(403);
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function a_question_can_be_deleted()
    {
        $this->withoutExceptionHandling();

        $question = factory(Question::class)->create(['user_id' => $this->user->id]);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'AUTHORIZATION' => 'Bearer ' . $this->user->access_token,
        ])->delete('/api/questions/' . $question->id);

        $this->assertCount(0, Question::all());

        $response->assertStatus(Response::HTTP_NO_CONTENT);
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function only_the_owner_can_delete_the_question()
    {
        $question = factory(Question::class)->create();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'AUTHORIZATION' => 'Bearer ' . $this->user->access_token,
        ])->delete('/api/questions/' . $question->id);

        $response->assertStatus(403);
    }


    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function a_list_of_questions_can_be_fetched_for_authenticated_user()
    {
        $this->withoutExceptionHandling();

        $user = factory(User::class)->create();
        $anotherUser = factory(User::class)->create();

        $question = factory(Question::class)->create(['user_id' => $user->id]);
        $anotherQuestion = factory(Question::class)->create(['user_id' => $anotherUser->id]);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'AUTHORIZATION' => 'Bearer ' . $this->user->access_token,
        ])->get('/api/questions');

        $response->assertJsonCount(1)
            ->assertJson([
                'data' => [
                    [
                        'data' => [
                            'question_id' => $question->id
                        ]
                    ]
                ]
            ]);
    }


    /**
     * @return string[]
     */
    private function data(): array
    {
        return [
            'question' => 'Question?',
            'answer' => 'Answer',
        ];
    }
}
