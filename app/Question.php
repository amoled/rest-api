<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = ['question', 'answer'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    //Get the resource path
    public function path()
    {
        return '/questions/' . $this->id;
    }

    public function scopeQuestion($query, $question)
    {
        if($question) {
            return $query->where('question', 'LIKE', '%' . $question . '%');
        }
    }

    public function scopeAnswer($query, $answer)
    {
        if($answer) {
            return $query->where('answer', 'LIKE', '%' . $answer . '%');
        }
    }
}
