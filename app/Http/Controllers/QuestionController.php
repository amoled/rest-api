<?php

namespace App\Http\Controllers;

use App\Question;
use App\Http\Resources\QuestionResource as QuestionResource;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class QuestionController extends Controller
{
    public function index()
    {
        $this->authorize('viewAny', Question::class);

        $questions = Question::Question(request('question'))
            ->Answer(request('answer'))
            ->orderBy('id', 'DESC')
            ->get();

        return QuestionResource::collection($questions);
    }

    public function store()
    {
        $this->authorize('create', Question::class);

        $Question = request()->user()->question()->create($this->validatedData());

        return (new QuestionResource($Question))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Question $Question)
    {
        $this->authorize('view', $Question);

        return new QuestionResource($Question);
    }

    public function update(Question $Question)
    {
        $this->authorize('update', $Question);

        $data = $this->validatedData();

        $Question->update($data);

        return (new QuestionResource($Question))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    public function destroy(Question $Question)
    {
        $this->authorize('delete', $Question);

        $Question->delete();

        return response(['data' => 'Successfully deleted the question'], Response::HTTP_NO_CONTENT);
    }

    /**
     * @return array
     */
    private function validatedData(): array
    {
        return request()->validate([
            'question' => 'required',
            'answer' => 'required',
        ]);
    }
}
